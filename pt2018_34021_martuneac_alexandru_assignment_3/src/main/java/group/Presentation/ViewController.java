package group.Presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewController {
    View view;


    /**
     * Constructor
     * @param view
     */
    public ViewController(View view)
    {
        this.view = view;

        this.view.addCalculationListener(new CalculateListener());

    }


    class CalculateListener implements ActionListener
    {
        /**
         * Button Events
         * @param arg0
         */
        public void actionPerformed(ActionEvent arg0)
        {
            if(arg0.getSource() == view.getCustomer())
            {
                view.dispose();
                ViewCustomer view1 = new ViewCustomer();
                ViewCustomerController controller1 = new ViewCustomerController(view1);
                view1.setVisible(true);
            }

            if(arg0.getSource() == view.getOrders())
            {
                view.dispose();
                ViewOrder view3 = new ViewOrder();
                ViewOrderController controller3 = new ViewOrderController(view3);
                view3.setVisible(true);
            }

            if(arg0.getSource() == view.getProducts())
            {
                view.dispose();
                ViewProduct view2 = new ViewProduct();
                ViewProductController controller2 = new ViewProductController(view2);
                view2.setVisible(true);
            }
        }
    }
}
