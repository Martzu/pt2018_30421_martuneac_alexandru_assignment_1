package group.Presentation;

import group.DataAccess.CustomerDAO;
import group.Model.Customer;
import sun.rmi.transport.ObjectTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Objects;

public class ViewCustomer extends JFrame{



    JButton back = new JButton("Main menu");
    JButton insert = new JButton("Insert");
    JButton bill = new JButton("Bill");
    JButton delete = new JButton("Delete");
    JButton update = new JButton("Update");
    JButton findall = new JButton("Find All");
    JButton findID = new JButton("Find by ID:");


    JLabel cage = new JLabel("Age");
    JLabel message = new JLabel("");
    JLabel sucessMessage = new JLabel("");
    JLabel foundById = new JLabel("");
    JLabel cname = new JLabel("Name");
    JLabel ocid = new JLabel("ID");
    JLabel caddress = new JLabel("Address");


    JTextField age = new JTextField(7);
    JTextField id = new JTextField(7);
    JTextField name = new JTextField(7);
    JTextField address = new JTextField(7);

    JTextField billAge = new JTextField(7);
    JTextField billId = new JTextField(7);
    JTextField billName = new JTextField(7);
    JTextField billAddress = new JTextField(7);

    JTextField updateAge = new JTextField(7);
    JTextField updateId = new JTextField(7);
    JTextField updateName = new JTextField(7);
    JTextField updateAddress = new JTextField(7);

    JTextField deleteID = new JTextField(7);


    //DefaultTableModel model = new DefaultTableModel(0,w.getClass().getDeclaredFields().length);

    /**
     * Starts the customer window
     */
    public ViewCustomer()
    {


        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1000,900);
        this.setVisible(true);


        CustomerDAO customerDAO = new CustomerDAO();

        ArrayList<Customer> objects = customerDAO.findAll();
        Customer w = new Customer();
        Header header = new Header<Customer>(customerDAO.findAll(),w);

        //JTable jtable1 = new JTable(header.getValues(),header.getParameters());
        //listAll();



        /*ArrayList<Customer> customers = customerDAO.findAll();
        for(int i = 0; i < customers.size(); i++)
        {
            Object rowData[] = new Object[w.getClass().getDeclaredFields().length];
            int k = 0;
            rowData[k++] = customers.get(i).getId();
            rowData[k++] = customers.get(i).getName();
            rowData[k++] = customers.get(i).getAddress();
            rowData[k++] = customers.get(i).getAge();
            model.addRow(rowData);
        }*/



        c.insets = new Insets(10,10,10,10);

        panel.add(message,c);


        //c.gridx = 3;
        //panel.add(jtable1,c);

        c.gridx = 0;

        c.gridy = 1;
        panel.add(findall,c);

        c.gridx = 1;
        panel.add(ocid,c);

        c.gridx = 2;
        panel.add(cname,c);

        c.gridx = 3;
        panel.add(caddress,c);

        c.gridx = 4;
        panel.add(cage,c);


        c.gridx = 0;

        c.gridy = 2;
        panel.add(update,c);

        c.gridx = 1;
        panel.add(updateId,c);

        c.gridx = 2;
        panel.add(updateName,c);

        c.gridx = 3;
        panel.add(updateAddress,c);

        c.gridx = 4;
        panel.add(updateAge,c);


        c.gridx = 0;

        c.gridy = 3;
        panel.add(delete,c);

        c.gridx = 1;
        panel.add(deleteID,c);

        c.gridx = 0;

        c.gridy = 4;
        panel.add(back,c);

        c.gridy = 5;
        panel.add(bill,c);

        c.gridx = 1;
        panel.add(billId,c);

        c.gridx = 2;
        panel.add(billName,c);

        c.gridx = 3;
        panel.add(billAddress,c);

        c.gridx = 4;
        panel.add(billAge,c);

        c.gridx = 0;

        c.gridy = 6;
        panel.add(insert,c);

        c.gridy = 7;
        panel.add(findID,c);

        c.gridx = 1;
        panel.add(id,c);

        c.gridx = 2;
        panel.add(foundById,c);



        c.gridx = 1;



        c.gridy = 8;
        panel.add(sucessMessage,c);

        c.gridy = 6;

        c.gridx = 2;
        panel.add(name,c);

        c.gridx = 3;
        panel.add(address,c);

        c.gridx = 4;
        panel.add(age,c);

        JTable jTable = new JTable(header.getValues(),header.getParameters());
        c.gridy = 9;
        JScrollPane tab = new JScrollPane(jTable);
        panel.add(tab,c);



        this.add(panel);


    }

    /**
     * Button Events
     * @param listenForCalcButton
     */

    public void addCalculationListener(ActionListener listenForCalcButton)
    {
        back.addActionListener(listenForCalcButton);
        bill.addActionListener(listenForCalcButton);
        insert.addActionListener(listenForCalcButton);
        delete.addActionListener(listenForCalcButton);
        update.addActionListener(listenForCalcButton);
        findID.addActionListener(listenForCalcButton);
        findall.addActionListener(listenForCalcButton);

    }

    public void listAll()
    {



    }


    /**
     *
     * @return JButton
     */
    public JButton getBack() {
        return back;
    }

    /**
     *
     * @return JButton
     */
    public JButton getInsert() {
        return insert;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getId() {
        return id;
    }

    /**
     *
     * @return JTextField
     */

    public JTextField getAddress() {
        return address;
    }

    /**
     *
     * @return JTextField
     */

    public JTextField getNam() {
        return name;
    }

    /**
     *
     * @return JLabel
     */

    public JLabel getMessage() {
        return message;
    }

    /**
     *
     * @return JLabeL
     */
    public JButton getBill() {
        return bill;
    }


    /**
     *
     * @return JButton
     */
    public JButton getDelete() {
        return delete;
    }

    /**
     *
     * @return JButton
     */

    public JButton getUpdate() {
        return update;
    }

    /**
     *
     * @return JLabel
     */

    public JLabel getSucessMessage() {
        return sucessMessage;
    }

    /**
     *
     * @return JButton
     */

    public JButton getFindall() {
        return findall;
    }

    /**
     *
     * @return JButton
     */
    public JButton getFindID() {
        return findID;
    }

    /**
     *
     * @return JLabel
     */

    public JLabel getFoundById() {
        return foundById;
    }

    /**
     *
     * @param foundById
     */
    public void setFoundById(JLabel foundById) {
        this.foundById = foundById;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getAge() {
        return age;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getUpdateAge() {
        return updateAge;
    }


    /**
     *
     * @return JTextField
     */
    public JTextField getUpdateId() {
        return updateId;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getUpdateName() {
        return updateName;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getUpdateAddress() {
        return updateAddress;
    }

    /**
     *
     * @return JTextField
     */

    public JTextField getDeleteID() {
        return deleteID;
    }


    /**
     *
     * @return JTextField
     */
    public JTextField getBillId() {
        return billId;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getBillAge() {
        return billAge;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getBillName() {
        return billName;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getBillAddress() {
        return billAddress;
    }
}
