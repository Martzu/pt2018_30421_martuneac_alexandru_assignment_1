package group.Presentation;

import com.mysql.cj.x.protobuf.MysqlxCrud;
import group.DataAccess.CustomerDAO;
import group.DataAccess.OrdersDAO;
import group.DataAccess.ProductDAO;
import group.Model.Customer;
import group.Model.Orders;
import group.Model.Product;
import sun.rmi.transport.ObjectTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Objects;

public class ViewOrder extends JFrame{



    JButton back = new JButton("Main menu");
    JButton insert = new JButton("Insert");
    JButton delete = new JButton("Delete");
    JButton update = new JButton("Update");
    JButton findall = new JButton("Find All");
    JButton findID = new JButton("Find by ID:");


    JLabel cquantity = new JLabel("Quantity");
    JLabel message = new JLabel("");
    JLabel underStock = new JLabel("");
    JLabel foundById = new JLabel("");
    JLabel cpid = new JLabel("Product");
    JLabel ccid = new JLabel("Customer");
    JLabel ocid = new JLabel("ID");



    JTextField customer = new JTextField(7);
    JTextField id = new JTextField(7);
    JTextField quantity = new JTextField(7);
    JTextField product = new JTextField(7);


    JTextField updateProduct = new JTextField(7);
    JTextField updateID = new JTextField(7);
    JTextField updateQuantity = new JTextField(7);
    JTextField updateCustomer = new JTextField(7);

    JTextField deleteID = new JTextField(7);


    //DefaultTableModel model = new DefaultTableModel(0,w.getClass().getDeclaredFields().length);

    /**
     * start order windows
     */
    public ViewOrder()
    {


        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1000,900);
        this.setVisible(true);

        OrdersDAO ordersDAO = new OrdersDAO();

        Orders w = new Orders();
        Header header = new Header<Orders>(ordersDAO.findAll(),w);

        //JTable jtable1 = new JTable(header.getValues(),header.getParameters());
        //listAll();



        /*ArrayList<Customer> customers = customerDAO.findAll();
        for(int i = 0; i < customers.size(); i++)
        {
            Object rowData[] = new Object[w.getClass().getDeclaredFields().length];
            int k = 0;
            rowData[k++] = customers.get(i).getId();
            rowData[k++] = customers.get(i).getName();
            rowData[k++] = customers.get(i).getAddress();
            rowData[k++] = customers.get(i).getAge();
            model.addRow(rowData);
        }*/



        c.insets = new Insets(10,10,10,10);

        panel.add(message,c);


        //c.gridx = 3;
        //panel.add(jtable1,c);

        c.gridx = 0;

        c.gridy = 1;
        panel.add(findall,c);

        c.gridx = 1;
        panel.add(ocid,c);

        c.gridx = 4;
        panel.add(cquantity,c);

        c.gridx = 2;
        panel.add(ccid,c);

        c.gridx = 3;
        panel.add(cpid,c);


        c.gridx = 0;

        c.gridy = 2;
        panel.add(update,c);

        c.gridx = 1;
        panel.add(updateID,c);

        c.gridx = 2;
        panel.add(updateCustomer,c);

        c.gridx = 3;
        panel.add(updateProduct,c);

        c.gridx = 4;
        panel.add(updateQuantity,c);


        c.gridx = 0;

        c.gridy = 3;
        panel.add(delete,c);

        c.gridx = 1;
        panel.add(deleteID,c);

        c.gridx = 0;

        c.gridy = 4;
        panel.add(back,c);

        c.gridx = 0;

        c.gridy = 6;
        panel.add(insert,c);

        c.gridx = 2;
        panel.add(customer,c);

        c.gridx = 3;
        panel.add(product,c);

        c.gridx = 4;
        panel.add(quantity,c);

        c.gridx = 0;

        c.gridy = 7;
        panel.add(findID,c);

        c.gridx = 1;
        panel.add(id,c);

        c.gridx = 2;
        panel.add(foundById,c);



        c.gridx = 1;



        c.gridy = 8;
        panel.add(underStock,c);

        c.gridx = 0;
        JTable jTable = new JTable(header.getValues(),header.getParameters());
        c.gridy = 9;
        JScrollPane tab = new JScrollPane(jTable);
        panel.add(tab,c);



        this.add(panel);


    }

    public void addCalculationListener(ActionListener listenForCalcButton)
    {
        back.addActionListener(listenForCalcButton);
        insert.addActionListener(listenForCalcButton);
        delete.addActionListener(listenForCalcButton);
        update.addActionListener(listenForCalcButton);
        findID.addActionListener(listenForCalcButton);
        findall.addActionListener(listenForCalcButton);

    }


    /**
     *
     * @return JButton
     */
    public JButton getBack() {
        return back;
    }


    /**
     *
     * @return JButton
     */
    public JButton getInsert() {
        return insert;
    }

    /**
     *
     * @return JButton
     */

    public JButton getDelete() {
        return delete;
    }

    /**
     *
     * @return JButton
     */
    public JButton getUpdate() {
        return update;
    }

    /**
     *
     * @return JButton
     */

    public JButton getFindall() {
        return findall;
    }

    /**
     *
     * @return JButton
     */
    public JButton getFindID() {
        return findID;
    }

    /**
     *
     * @return JLabel
     */
    public JLabel getCquantity() {
        return cquantity;
    }

    /**
     *
     * @return JLabel
     */
    public JLabel getMessage() {
        return message;
    }

    /**
     *
     * @return JLabel
     */
    public JLabel getUnderStock() {
        return underStock;
    }

    /**
     *
     * @return JLabel
     */
    public JLabel getFoundById() {
        return foundById;
    }

    /**
     *
     * @return JLabel
     */
    public JLabel getCpid() {
        return cpid;
    }

    /**
     *
     * @return JLabel
     */
    public JLabel getCcid() {
        return ccid;
    }

    /**
     *
     * @return JLabel
     */
    public JLabel getOcid() {
        return ocid;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getCustomer() {
        return customer;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getId() {
        return id;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getQuantity() {
        return quantity;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getProduct() {
        return product;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getUpdateProduct() {
        return updateProduct;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getUpdateID() {
        return updateID;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getUpdateQuantity() {
        return updateQuantity;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getUpdateCustomer() {
        return updateCustomer;
    }

    /**
     *
     * @return JTextField
     */
    public JTextField getDeleteID() {
        return deleteID;
    }
}
