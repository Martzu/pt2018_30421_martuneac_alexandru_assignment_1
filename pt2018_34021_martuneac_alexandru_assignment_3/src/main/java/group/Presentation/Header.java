package group.Presentation;


import com.mysql.cj.x.protobuf.MysqlxDatatypes;
import group.DataAccess.AbstractDAO;

import javax.swing.*;
import javax.swing.plaf.nimbus.AbstractRegionPainter;
import javax.swing.table.DefaultTableModel;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Header<T> {

    String[] parameters;
    Object[][] values;


    /**
     * Constructor
     * @param objects
     * @param object
     */
    public Header(ArrayList<T> objects, Object object)
    {
        parameters = new String[object.getClass().getDeclaredFields().length];
        values = new Object[objects.size()][object.getClass().getDeclaredMethods().length];
        int k = 0;
        for(Field x : object.getClass().getDeclaredFields())
        {
            //int index = x.toString().lastIndexOf(".");
            //String parameter = x.toString().substring(index + 1);
            parameters[k++] = x.getName();
        }


        for(int i = 0; i < objects.size(); i++)
        {
            int j = 0;

            for(Field x : object.getClass().getDeclaredFields())
            {
                x.setAccessible(true);
                try {
                    values[i][j++] = x.get(objects.get(i));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }


        }

    }

    /**
     *
     * @return String[]
     */
    public String[] getParameters() {
        return parameters;
    }

    /**
     *
     * @return Object[][]
     */

    public Object[][] getValues() {
        return values;
    }
}
