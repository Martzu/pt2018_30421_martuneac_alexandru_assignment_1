package group.BussinessLogic;

import group.DataAccess.OrdersDAO;
import group.DataAccess.ProductDAO;
import group.Model.Orders;
import group.Model.Product;
import jdk.nashorn.internal.objects.annotations.Property;

import java.util.ArrayList;

public class ValidateOrd{

    private OrdersDAO ordersDAO = new OrdersDAO();
    private ProductDAO productDAO = new ProductDAO();
    private ArrayList<Product> products;
    private ArrayList<Orders> orders;

    /**
     * Constructor
     */

    public ValidateOrd()
    {
        orders = new ArrayList<>();
        orders = ordersDAO.findAll();
        products = productDAO.findAll();
    }


    /**
     *
     * @param order
     * @return true
     */
    public boolean isValid(Orders order)
    {
        for(Orders x : orders)
        {
            if (x.getId() == order.getId())
            {
                return false;
            }
        }
        for(Product y : products)
        {
            if(y.getId() == order.getPid())
            {
                if(y.getQuantity() < order.getQuantity())
                {
                    return false;
                }
            }
        }
        ordersDAO.insert(order);//afisez pe un jlabel, Invalid entry, ID already exists
        return true;
    }

    /**
     *
     * @param order
     * @return false
     */

    public boolean exists(Orders order)
    {
        for(Orders x : orders)
        {
            if(x.getId() == order.getId())
            {
                return true;
            }
        }
        return false;
    }


}
