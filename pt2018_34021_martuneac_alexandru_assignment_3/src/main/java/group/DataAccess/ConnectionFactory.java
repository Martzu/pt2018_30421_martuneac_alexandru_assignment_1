package group.DataAccess;

import java.sql.*;
import java.util.logging.Logger;

public class ConnectionFactory {

    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/warehouse?serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASS = "qwerty1997";

    private static ConnectionFactory singleInstance = new ConnectionFactory();

    /**
     * Constructor
     */
    private ConnectionFactory()
    {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return Connection
     */

    public Connection createConnection()
    {

        try {
            Connection connection = DriverManager.getConnection(DBURL, USER, PASS);
            return connection;
        } catch (SQLException e) {
            System.out.println("Unable to connect to database");
        }

        return null;
    }

    /**
     *
     * @return Connection
     */
    public Connection getConnection()
    {
        return singleInstance.createConnection();
    }

    /**
     *
     * @param connection
     */
    public void close(Connection connection)
    {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     *
     * @return ConnectionFactory
     */
    public static ConnectionFactory getInstance()
    {
        return singleInstance;
    }

    /**
     *
     * @param statement
     */
    public void close(Statement statement)
    {
        try {
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param resultSet
     */
    public void close(ResultSet resultSet)
    {
        try {
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
