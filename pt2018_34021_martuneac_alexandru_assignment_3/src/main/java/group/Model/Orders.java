package group.Model;

public class Orders {

    private int id;
    private int cid;
    private int pid;
    private int quantity;

    /**
     *
     * @param id
     * @param cid
     * @param pid
     * @param quantity
     */
    public Orders(int id, int cid, int pid, int quantity)
    {
        this.id = id;
        this.cid = cid;
        this.pid = pid;
        this.quantity = quantity;
    }

    /**
     * Empty Constructor
     */
    public Orders()
    {

    }

    /**
     *
     * @return int
     */

    public int getQuantity() {
        return quantity;
    }


    /**
     *
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     *
     * @return int
     */
    public int getCid() {
        return cid;
    }

    /**
     *
     * @param cid
     */

    public void setCid(int cid) {
        this.cid = cid;
    }

    /**
     *
     * @return int
     */
    public int getPid() {
        return pid;
    }

    /**
     *
     * @param pid
     */

    public void setPid(int pid) {
        this.pid = pid;
    }

    /**
     *
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return String
     */
    public String toString()
    {
        return Integer.toString(id) +  " " + Integer.toString(cid) +  " " + Integer.toString(pid) + " " + Integer.toString(quantity);
    }

}
