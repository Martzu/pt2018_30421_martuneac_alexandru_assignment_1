package PT2018.Homework1.Controller;

import PT2018.Homework1.View.*;
import PT2018.Homework1.Model.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private View theView;
    String[] aux = new String[2];

    /**
     * receives the object that will represen the GUI to work on
     * @param theView
     */
    public Controller(View theView)
    {
        this.theView = theView;
        this.theView.addCalculationListener(new CalculateListener());
    }

    class CalculateListener implements ActionListener
    {
        /**
         * which button was clicked
         * @param arg0
         */
        public void actionPerformed(ActionEvent arg0)
        {
            if(arg0.getSource() == theView.getAddButton())
            {
                PolynomModel pol1 = new PolynomModel(theView.getFirstPolynom());
                PolynomModel pol2 = new PolynomModel(theView.getSecondPolynom());
                PolynomModel pol3 = new PolynomModel(pol1.add(pol2));
                theView.setCalcSolution(pol3.toString());
                theView.setAuxSolution("");
            }
            if(arg0.getSource() == theView.getDerButton())
            {
                PolynomModel pol4 = new PolynomModel(theView.getpolSpecial());
                pol4.derivate();
                theView.setSpecialSolution(pol4.toString());

            }
            if(arg0.getSource() == theView.getDifButton())
            {
                PolynomModel pol1 = new PolynomModel(theView.getFirstPolynom());
                PolynomModel pol2 = new PolynomModel(theView.getSecondPolynom());
                PolynomModel pol3 = new PolynomModel(pol1.substract(pol2));
                theView.setCalcSolution(pol3.toString());
                theView.setAuxSolution("");
            }
            if(arg0.getSource() == theView.getDivButton())
            {
                aux[0] = "";
                aux[1] = "";
                //to properly display remainder and quotient
                PolynomModel pol1 = new PolynomModel(theView.getFirstPolynom());
                PolynomModel pol2 = new PolynomModel(theView.getSecondPolynom());
                if(pol2.getPol().size() != 0)
                {
                    PolynomModel pol3 = new PolynomModel(pol1.divide(pol2));
                    aux = pol3.show();
                }
                theView.setCalcSolution(aux[0]);
                theView.setAuxSolution(aux[1]);
            }
            if(arg0.getSource() == theView.getMulButton())
            {
                PolynomModel pol1 = new PolynomModel(theView.getFirstPolynom());
                PolynomModel pol2 = new PolynomModel(theView.getSecondPolynom());
                PolynomModel pol3 = new PolynomModel(pol1.mul(pol2));
                theView.setCalcSolution(pol3.toString());
                theView.setAuxSolution("");
            }
            if(arg0.getSource() == theView.getIntButton())
            {
                PolynomModel pol4 = new PolynomModel(theView.getpolSpecial());
                pol4.integrate();
                theView.setSpecialSolution(pol4.toString());

            }

        }
    }

}


