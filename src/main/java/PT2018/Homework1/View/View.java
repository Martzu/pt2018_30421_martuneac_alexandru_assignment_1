package PT2018.Homework1.View;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class View extends JFrame{

    private JTextField firstPolynom = new JTextField(20);
    private JTextField secondPolynom = new JTextField(20);
    private JTextField calcSolution = new JTextField(20);

    private JTextField auxSolution = new JTextField(20);
    private JTextField specialSolution = new JTextField(20);
    private JTextField polSpecial = new JTextField(20);

    private JButton addButton = new JButton("Add");
    private JButton derButton = new JButton("Differentiate");
    private JButton intButton = new JButton("Integrate");
    private JButton difButton = new JButton("Substract");
    private JButton mulButton = new JButton("Multiply");
    private JButton divButton = new JButton("Divide");

    private JLabel specialpol = new JLabel("Polynom to derivate or integrate");
    private JLabel specialop = new JLabel("Result of differentiation or integration");
    private JLabel polynom1 = new JLabel("Polynom 1");
    private JLabel polynom2 = new JLabel("Polynom 2");
    private JLabel quotient = new JLabel("Result");
    private JLabel remainder = new JLabel("Remainder");


    /**
     * responsibile for how the GUI looks
     */
    public View()
    {
        super("Polynom calculator");
        JPanel calcPanel = new JPanel(new GridBagLayout());

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(800,600);
        GridBagConstraints c = new GridBagConstraints();


        c.insets = new Insets(0,10,0,10);
        calcPanel.add(polynom1,c);
        calcPanel.add(firstPolynom,c);


        c.gridx = 0;
        c.gridy = 2;
        c.insets = new Insets(20,10,20,10);
        calcPanel.add(polynom2,c);
        c.gridx = -1;
        c.gridy = 2;
        calcPanel.add(secondPolynom,c);

        //cresti y mergi in jos
        //scazi y mergi in sus

        c.gridx = 0;
        c.gridy = 3;
        calcPanel.add(quotient,c);

        c.gridx = -1;
        c.gridy = 3;
        calcSolution.setEditable(false);
        calcPanel.add(calcSolution,c);

        c.gridx = 0;
        c.gridy = 4;
        calcPanel.add(remainder,c);

        c.gridx = -1;
        auxSolution.setEditable(false);
        calcPanel.add(auxSolution,c);

        c.insets = new Insets(40,10,40,10);
        c.gridx = 0;
        c.gridy = 5;
        calcPanel.add(specialpol,c);

        c.insets = new Insets(20,10,20,10);
        c.gridx = -1;
        c.gridy = 5;
        calcPanel.add(polSpecial,c);

        c.gridx = 0;
        c.gridy = 6;
        calcPanel.add(specialop,c);

        c.gridx = -1;
        c.gridy = 6;
        specialSolution.setEditable(false);
        calcPanel.add(specialSolution,c);

        c.gridx = 4;
        c.gridy = 0;
        c.insets = new Insets(10,10,10,10);
        calcPanel.add(addButton,c);

        c.gridy = 1;
        calcPanel.add(difButton,c);

        c.gridy = 2;
        calcPanel.add(mulButton,c);

        c.gridy = 3;
        calcPanel.add(divButton,c);

        c.gridy = 5;
        calcPanel.add(derButton,c);

        c.gridy = 6;
        calcPanel.add(intButton,c);


        this.add(calcPanel);
    }

    /**
     * returns the solution of derivation or integration
     * @return specialSolution.getText()
     */
    public String getSpecialSolution()
    {
        return specialSolution.getText();
    }

    /**
     * sets the solution for derivation or integration
     * @param poly
     */
    public void setSpecialSolution(String poly)
    {
        specialSolution.setText(poly);
}

    /**
     * gets the first polynom to be parsed
     * @return firstPolynom.getText()
     */
    public String getFirstPolynom()
    {
        return firstPolynom.getText();
    }

    /**
     * gets the second polynom to be parsed
     * @return
     */
    public String getSecondPolynom()
    {
        return secondPolynom.getText();
    }

    /**
     * gets the polynom that will be integrated or differentiated to be parsed
     * @return polSpecial.getText()
     */
    public String getpolSpecial()
    {
        return polSpecial.getText();
    }

    /**
     * sets the polynom that will be integrated or differentiated to be parsed
     * @param poly
     */
    public void setPolSpecial(String poly)
    {
       polSpecial.setText(poly);
    }

    /**
     * sets the remainder of the division between 2 polynoms
     * @param poly
     */
    public void setAuxSolution(String poly)
    {
        auxSolution.setText(poly);
    }

    /**
     * sets the reminder of the divison, or the result from addition, substraction, multiplication
     * @param poly
     */
    public void setCalcSolution(String poly)
    {
        calcSolution.setText(poly);
    }

    /**
     * listener so the functions will be called based on clicks of the buttons
     * @param listenForCalcButton
     */

    public void addCalculationListener(ActionListener listenForCalcButton)
    {
        addButton.addActionListener(listenForCalcButton);
        difButton.addActionListener(listenForCalcButton);
        mulButton.addActionListener(listenForCalcButton);
        divButton.addActionListener(listenForCalcButton);
        intButton.addActionListener(listenForCalcButton);
        derButton.addActionListener(listenForCalcButton);
    }

    /**
     * get the button along with its source
     * @return addButton
     */
    public javax.swing.JButton getAddButton() {
        return addButton;
    }

    /**
     * get the button along with its source
     * @return difButton
     */

    public javax.swing.JButton getDifButton() {
        return difButton;
    }

    /**
     * get the button along with its source
     * @return mulButton
     */

    public javax.swing.JButton getMulButton() {
        return mulButton;
    }

    /**
     * get the button along with its source
     * @return divButton
     */
    public javax.swing.JButton getDivButton() {
        return divButton;
    }

    /**
     * get the button along with its source
     * @return derButton
     */

    public javax.swing.JButton getDerButton() {
        return derButton;
    }

    /**
     * get the button along with its source
     * @return intButton
     */

    public javax.swing.JButton getIntButton() {
        return intButton;
    }


}
