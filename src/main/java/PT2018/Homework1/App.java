package PT2018.Homework1;
import PT2018.Homework1.View.*;
import PT2018.Homework1.Controller.*;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main(String[] args)
    {
        View theView = new View();
        Controller controller = new Controller(theView);

        theView.setVisible(true);
    }
}
