package PT2018.Homework1.Model;

public class MonomModel {


    private FractionModel zero = new FractionModel(0);
    private FractionModel coefficient;
    private FractionModel power;
    private FractionModel one = new FractionModel(1);


    /**
     * create monom from integers
     * @param coefficient
     * @param power
     */
    public MonomModel(int coefficient, int power)
    {
        this.power = new FractionModel(power);
        this.coefficient = new FractionModel(coefficient);
    }

    /**
     * create monom from fraction and integer
     * @param coefficient
     * @param power
     */

    public MonomModel(FractionModel coefficient, int power)
    {
        this.power = new FractionModel(power);
        this.coefficient = coefficient;
    }

    /**
     * gets the coefficient of the monom
     * @return this.coefficient
     */

    public FractionModel getCoefficient() {
        return this.coefficient;
    }


    /**
     * return the addition of 2 monoms in the monom passed as argument
     * @param mon
     * @return mon
     */
    public MonomModel madd(MonomModel mon)
    {
        mon.setCoefficient(mon.getCoefficient().madd(this.getCoefficient()));
        return mon;
    }


    /**
     * changes the sign of the monom
     * @return this
     */
    public MonomModel mnega()
    {
        this.coefficient.negative();
        return this;
    }

    /**
     * return the substraction of 2 monoms in the monom passed as argument
     * @param mon
     * @return mon
     */
    public MonomModel msubstract(MonomModel mon)
    {
        mon.setCoefficient(this.getCoefficient().msubstract(mon.getCoefficient()));
        return mon;
    }

    /**
     * sets the coefficient
     * @param coefficient
     */

    public void setCoefficient(FractionModel coefficient) {
        this.coefficient.setNumerator(coefficient.getNumerator());
        this.coefficient.setDenominator(coefficient.getDenominator());
    }

    /**
     * sets the power
     * @param power
     */

    public void setPower(int power) {
        this.power.setNumerator(power);
    }

    /**
     * gets the power
     * @return power.getNumerator()
     */

    public int getPower() {
        return power.getNumerator();
    }

    /**
     * derivates the monom
     */
    public void derivate()
    {
        if (this.power.getNumerator() != 0)
        {
            this.coefficient.multiply(this.power);
            this.power.substract(one);
        }
        else
        {
            this.coefficient.setNumerator(0);
        }
    }

    /**
     * integrates the monom
     */

    public void integrate()
    {

        if (this.power.getNumerator() == 0)
        {
            this.power.setNumerator(1);
        }
        else
            {
                this.power.add(one);
                FractionModel fr = new FractionModel(1, power.getNumerator());
                this.coefficient.multiply(fr);
            }
    }

    /**
     * generates a string representation of the monom
     * @return s
     */
    public String ashow()
    {
        String s = "";
        if(this.getCoefficient().equals(one))
        {
            if(this.getPower() != 0)
            {
                if(this.getPower() == 1)
                {
                    s+="x";
                }
                else
                {
                    s+=("x^" + Integer.toString(this.getPower()));
                }

            }
            else
            {
                s+=Integer.toString(1);
            }
        }
        else
        {
            if(this.getPower() == 1)
            {
                s+=(this.getCoefficient().show() + "x");
            }
            else
            {
                if(this.getPower() == 0)
                {
                    s+=this.getCoefficient().show();
                }
                else
                {
                    s+=(this.getCoefficient().show() + "x^" + Integer.toString(this.getPower()));
                }
            }
        }
        return s;
    }

}
