package PT2018.Homework1.Model;

import java.util.ArrayList;
import java.util.Iterator;

public class PolynomModel {

    private ArrayList<MonomModel> poly;
    private ArrayList<MonomModel> aux = new ArrayList<MonomModel>();
    private FractionModel zero = new FractionModel(0);
    private FractionModel one = new FractionModel(1);
    private String[] pr = new String[2];
    private static int remIndex = 0;

    /**
     * creates polynom from arraylist of monoms
     * @param pl
     */

    public PolynomModel(ArrayList<MonomModel> pl)
    {
        poly = new ArrayList<MonomModel>();
        for(MonomModel m: pl)
        {
            if(!m.getCoefficient().equals(zero))
            {
                poly.add(m);
            }

        }

    }

    /**
     * creates polynom from a string(parser of the input)
     * @param polynom
     */

    public PolynomModel(String polynom)
    {
        poly = new ArrayList<MonomModel>();
        int v = 0;//v is used so we don't confuse some powers with coefficients
        boolean mpower = false;
        boolean parse = false;
        String terms[] = new String[2];// used to hold power and coefficient
        String elements[] = polynom.split("(\\+)|(-)");//use escape so + will be used as char, not its regex role
        for (String x : elements)
        {
            if (x.equals("") == false) {//when we split we get an additional "" which we should not process
                MonomModel mon;
                int j = 0;
                int minus = -1;//get the sign of the monom's coefficient
                int position = polynom.indexOf(x, v); //get the position of the monom starting from position v
                if (position == 0)//if x starts at position 0
                {
                    minus = 1;//monom is positive
                }
                else
                    {
                        if (polynom.charAt(position - 1) == '+')//if the position before x is +
                        {
                            minus = 1;//then the monom will be positive, otherwise negative
                        }
                }
                boolean onlydigits = true;
                for (int i = 0; i < x.length(); i++)
                {
                    if (x.charAt(i) == 'x')
                    {
                        onlydigits = false;
                    }
                }
                if (onlydigits)//case for when we have constants
                {
                    mon = new MonomModel(minus*Integer.parseInt(x),0);
                    poly.add(mon);
                }
                else
                    {//if the power is at least one
                    String parts[] = x.split("(x\\^)|(x)");//we split it
                    for (String p : parts) {
                        terms[j++] = p;
                        if(p.equals(""))//case when the monom's coefficient is 1 and "" will be contorized
                        {
                            j--;
                            mpower = true;
                        }
                    }
                    if (j == 0)//if the monom has only coefficient which is 1 or -1 and it's power is 1
                    {
                        mon = new MonomModel(minus,1);
                        poly.add(mon);
                    }
                    else
                        {
                            if (j == 1 && !mpower)//for power 1 and coefficient different than 1 or -1
                            {
                                mon = new MonomModel(minus*Integer.parseInt(terms[0]),1);
                                poly.add(mon);
                            }
                            else
                                {
                                    if(mpower)//if power is > 1 and coefficient is 1 or -1
                                    {
                                        mon = new MonomModel(minus,Integer.parseInt(terms[0]));
                                        poly.add(mon);
                                    }
                                    else//if power > 1 and coefiicient different than 1 or -1
                                    {
                                        mon = new MonomModel(minus*(Integer.parseInt(terms[0])),Integer.parseInt(terms[1]));
                                        poly.add(mon);
                                    }
                            }
                    }
                }
                if(v == 0 && polynom.charAt(0) == '-' )//so we can search for the next monom properly
                {
                    v++;
                }
                v += x.length();
                if((x.length() == 3 || x.length() == 4) && !parse)
                {
                    v++;//add only once 1 to the position from where we start, if we encounter monoms of size at least 3 or 4
                    parse = true;
                }
                mpower = false;
            }
        }
    }

    /**
     *transform remainder and quotient in strings
     * @return this.pr
     */

    public String[] show()
    {
        pr[0] = "";//use array of string to
        pr[1] = "";
        int i = 0;
        int index = 0;
        for (MonomModel mn: this.getPol())
        {
            if(index == this.remIndex)//when we reach the index corresponding to the position from where the remainder starts
            {
                i++;//we add the remainder in the next element of the array
            }
            if(mn.ashow().charAt(0) != '-')
            {
                if(i == 0)
                {
                    if(index != 0)
                    {
                        pr[i]+="+";
                    }
                }
                else
                {
                    if(index >= this.remIndex + 1)
                    {
                        pr[i]+="+";
                    }
                }
            }
            pr[i] += mn.ashow();
            index++;
        }
        return this.pr;
    }

    /**
     * integrates each monom of the polynom
     */

    public void integrate()
    {
        for(MonomModel mn: poly)
        {
            mn.integrate();
        }
        Iterator<MonomModel> iter = poly.iterator();
        while(iter.hasNext())
        {
            MonomModel mon = iter.next();
            if (mon.getCoefficient().equals(zero))
            {
                iter.remove();
            }
        }
    }

    /**
     * returns the monoms of the polynom
     * @return this.poly
     */
    public ArrayList<MonomModel> getPol()
    {
        return this.poly;
    }

    /**
     * sets the monoms of the polynom
     * @param pl
     */

    public void setPol(ArrayList<MonomModel> pl)
    {
        int index = 0;
        for(MonomModel m:this.poly)
        {
            m = pl.get(index++);
        }
    }

    /**
     * gets the greatest power from the monoms
     * @return max
     */
    public int maxpower()
    {
        int max = 0;
        for(MonomModel m : this.getPol())
        {
            if (m.getPower() > max)
            {
                max = m.getPower();
            }
        }
        return max;
    }

    /**
     * returns the monom with the greatest power
     * @param max
     * @return mon
     */
    public MonomModel mpMonom(int max)
    {
        MonomModel mon = new MonomModel(1,1);
        for (MonomModel mn: this.getPol())
        {
            if (mn.getPower() == max)
            {
                mon.setCoefficient(mn.getCoefficient());
                mon.setPower(mn.getPower());
            }
        }
        return mon;
    }

    /**
     * derivates the monoms
     */

    public void derivate()
    {
        for(MonomModel mn: poly)
        {
            mn.derivate();
        }
        Iterator<MonomModel> iter = poly.iterator();
        while(iter.hasNext())
        {
            MonomModel mon = iter.next();
            if (mon.getCoefficient().equals(zero))
            {
                iter.remove();
            }
        }
    }

    /**
     * adds a polynom with another one
     * @param pol2
     * @return this.aux
     */

    public ArrayList<MonomModel> add(PolynomModel pol2) {
        ArrayList<MonomModel> p2aux = new ArrayList<MonomModel>();
        p2aux = pol2.getPol();
        boolean found = false;
        MonomModel mn = new MonomModel(one, 1);
        for (MonomModel mon : this.getPol())
        {
            boolean introduced = false;
            for (MonomModel mn2 : pol2.getPol())
            {
                found = false;
                if (mn2.getPower() == mon.getPower())
                {
                    MonomModel mux = new MonomModel(mon.madd(mn2).getCoefficient(),mon.getPower());
                    aux.add(mux);
                    found = true;
                    introduced = true;
                }
            }
            if (!found && !introduced)
            {
                aux.add(mon);
            }
        }
        pol2.setPol(p2aux);
        int k = -1;
        int index;
        for (MonomModel mn2 : pol2.getPol())
        {
            found = false;
            index = ++k;
            for (MonomModel mon : this.getPol())
            {
                if (mn2.getPower() == mon.getPower())
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                aux.add(pol2.getPol().get(index));
            }
        }
        return this.aux;
    }

    /**
     * substracts 2 polynoms
     * @param pol2
     * @return this.aux
     */

    public ArrayList<MonomModel> substract(PolynomModel pol2) {
        ArrayList<MonomModel> p2aux = new ArrayList<MonomModel>();
        p2aux = pol2.getPol();
        boolean found = false;
        MonomModel mn = new MonomModel(one, 1);
        for (MonomModel mon : this.getPol())
        {
            boolean introduced = false;
            for (MonomModel mn2 : pol2.getPol())
            {
                found = false;
                if (mn2.getPower() == mon.getPower())
                {
                    MonomModel mux = new MonomModel(mon.msubstract(mn2).getCoefficient(),mon.getPower());
                    aux.add(mux);
                    found = true;
                    introduced = true;
                    //mn2.setCoefficient(maux.getCoefficient());
                }
            }
            if (!found && !introduced)//cand in primu polinom am monoame cu grade diferite de cele din polinomul al doile
            {
                aux.add(mon);
            }
        }
        pol2.setPol(p2aux);
        int k = -1;
        int index;
        for (MonomModel mn2 : pol2.getPol())
        {
            found = false;
            index = ++k;
            for (MonomModel mon : this.getPol())
            {
                if (mn2.getPower() == mon.getPower())
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                aux.add(pol2.getPol().get(index).mnega());
            }
        }
        pol2.setPol(p2aux);
        return this.aux;
    }

    /**
     * multiplies 2 polynoms
     * @param pol2
     * @return p1.getPol()
     */
    public ArrayList<MonomModel> mul(PolynomModel pol2)
    {
        PolynomModel pl;
        PolynomModel pi;
        ArrayList<MonomModel> p2 = pol2.getPol();
        ArrayList<MonomModel> aux = new ArrayList<MonomModel>();
        ArrayList<MonomModel> aux1 = new ArrayList<MonomModel>();
        ArrayList<MonomModel> aux2 = new ArrayList<MonomModel>();
        ArrayList<MonomModel> rez = new ArrayList<MonomModel>();
        for(MonomModel mon: this.poly)
        {
            for(MonomModel mno:pol2.getPol())
            {
                MonomModel mn = new MonomModel(mon.getCoefficient().mmultiply(mno.getCoefficient()),mon.getPower()+mno.getPower());
                aux.add(mn);
            }
        }
        aux1.add(aux.get(0));
        pl = new PolynomModel(aux1);
        for(int i = 1; i < aux.size(); i++)
        {
            aux2.add(aux.get(i));
            pi = new PolynomModel(aux2);
            pl = new PolynomModel(pl.add(pi));
            aux2.remove(aux.get(i));
        }

        return pl.getPol();
    }

    /**
     * returns an arraylist which contains the remainder and the quotient of the division between 2 polynoms
     * @param p2
     * @return aux
     */

    public ArrayList<MonomModel> divide(PolynomModel p2)
    {
        this.remIndex = 0;
        boolean rest = false;
        ArrayList<MonomModel> aux = new ArrayList<MonomModel>();
        ArrayList<MonomModel> aux2 = new ArrayList<MonomModel>();
        ArrayList<MonomModel> aux3 = p2.getPol();
        ArrayList<MonomModel> aux4 = this.getPol();
        PolynomModel p3 = new PolynomModel(aux4);
        while(p3.maxpower() != 0 && p3.maxpower() >= p2.maxpower())
        {
            MonomModel m = new MonomModel(p3.mpMonom(p3.maxpower()).getCoefficient().mdivide(p2.mpMonom(p2.maxpower()).getCoefficient()), p3.maxpower()-p2.maxpower());
            aux.add(m);//inmultesc pe m cu p2
            this.remIndex++;
            for(MonomModel mno: aux3)
            {
                MonomModel nm = new MonomModel(m.getCoefficient().mmultiply(mno.getCoefficient()),mno.getPower()+m.getPower());
                aux2.add(nm);
            }
            PolynomModel p = new PolynomModel(aux2);
            aux2.clear();

            p3 = new PolynomModel(p3.substract(p));
        }
        for(MonomModel x: p3.getPol())
        {
            aux.add(x);
        }
        return aux;
    }

    /**
     * creates a string representation of the polynom
     * @return s
     */
    public String toString()
    {
        String s = "";
        int ik = 0;
        for (MonomModel mn: this.getPol())
        {
            if(ik != 0 && mn.ashow().charAt(0) != '-')
            {
                s+="+";
            }
            s += mn.ashow();
            ik++;
        }
        return s;
    }
}
