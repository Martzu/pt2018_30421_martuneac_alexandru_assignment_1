package PT2018.Homework1.Model;

public class FractionModel {

    private int denominator;
    private int numerator;

    /**
     * creates a fraction without denominator
     * @param numerator
     */

    public FractionModel(int numerator)
    {
        this.denominator = 1;
        this.numerator = numerator;
    }

    /**
     * creates a fraction with both nominator and denominator
     * @param numerator
     * @param denominator
     */

    public FractionModel(int numerator, int denominator)
    {
        this.denominator = denominator;
        this.numerator = numerator;
        this.simplify();
    }

    /**
     * multiplies this by a fraction
     * @param fr
     */

    public void multiply(FractionModel fr)
    {
        this.denominator *= fr.denominator;
        this.numerator *= fr.numerator;
        this.simplify();
    }

    /**
     * multiplies this and a fraction and stores the result in another fraction
     * @param fr
     * @return aux
     */
    public FractionModel mmultiply(FractionModel fr)
    {
        FractionModel aux;
        int num = this.numerator;
        int den = this.denominator;
        num *= fr.getNumerator();
        den *= fr.getDenominator();
        aux = new FractionModel(num, den);
        this.simplify();//oricum se simplifica la constructor
        return aux;
    }

    /**
     * divides this and a fraction and stores the result in another fraction
     * @param fr
     * @return
     */

    public FractionModel mdivide(FractionModel fr)
    {
        FractionModel f = new FractionModel(this.getNumerator()*fr.getDenominator(),this.getDenominator()*fr.getNumerator());
        f.simplify();
        return f;
    }

    /**
     * checks if two fractions are equal
     * @param fr
     * @return true
     */

    public boolean equals(FractionModel fr)
    {
        if(this.numerator == fr.getNumerator() && this.denominator == fr.getDenominator())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * changes the sign of the numerator
     */
    public void negative()
    {
        this.numerator *=(-1);
    }

    /**
     * substracts 2 fractions and stores the result in another one
     * @param fr
     * @return faux
     */

    public FractionModel msubstract(FractionModel fr)
    {
        int denom = this.denominator;
        int num = this.numerator;
        int aux = denom;

        denom *= fr.denominator;
        num *= fr.denominator;

        FractionModel fr1 = new FractionModel(fr.getNumerator(),fr.getDenominator());
        fr1.setDenominator(fr1.denominator*aux);
        fr1.setNumerator(fr1.numerator*aux);

        FractionModel faux = new FractionModel(num-fr1.numerator,fr1.getDenominator());

        faux.simplify();

        return faux;
    }

    /**
     * adds a fraction with this
     * @param fr
     */

    public void add(FractionModel fr)
    {
        int a = this.denominator;
        int b = fr.getDenominator();

        this.denominator *= b;
        this.numerator *= b;

        fr.denominator *= a;
        fr.numerator *= a;

        this.numerator += fr.numerator;
        this.simplify();
    }

    /**
     * adds 2 fractions and stores the result in another one
     * @param fr
     * @return faux
     */

    public FractionModel madd(FractionModel fr)
    {
        FractionModel faux;
        FractionModel auxiliar = new FractionModel(this.numerator,this.denominator);

        //!= -1 strica
        if(fr.getDenominator() != 1)
        {
            faux = new FractionModel(auxiliar.msubstract(fr).getNumerator(),auxiliar.msubstract(fr).getNumerator());
        }
        else
        {
            int aux = this.denominator;
            int a = this.denominator;
            int b = fr.getDenominator();

            int f1 = fr.getNumerator();
            int f2 = fr.getDenominator();

            int a1 = this.numerator;

            a *= b;
            a1 *= b;

            f2 *= aux;
            f1 *= aux;

            a1 += f1;
            faux = new FractionModel(a1,a);


            this.simplify();
        }


        return faux;

    }

    /**
     * susbtracts a fraction from this
     * @param fr
     */

    public void substract(FractionModel fr)
    {
        int a = this.denominator;
        int b = fr.getDenominator();

        this.denominator *= b;
        this.numerator *= b;

        fr.denominator *= a;
        fr.numerator *= a;

        this.numerator -= fr.numerator;
        this.simplify();
    }

    /**
     * simplifies a fraction
     */

    public void simplify()
    {
        int b = this.denominator;
        int a = this.numerator;
        int r;
        while(b != 0)
        {
            r = a % b;
            a = b;
            b = r;
        }
        //System.out.println(a);
        this.numerator /= a;
        this.denominator /= a;
        if(this.numerator < 0 && this.denominator < 0)
        {
            this.numerator = Math.abs(this.numerator);
            this.denominator = Math.abs(this.denominator);
        }
        else
        {
            if (this.numerator > 0 && this.denominator < 0)
            {
                this.numerator = -this.numerator;
                this.denominator = Math.abs(denominator);
            }
        }
    }

    /**
     * gets the denominator
     * @return denominator
     */
    public int getDenominator() {
        return denominator;
    }

    /**
     * sets the denominator
     * @param denominator
     */
    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    /**
     * gets the numerator
     * @return numerator
     */

    public int getNumerator() {
        return numerator;
    }

    /**
     * sets the numerator
     * @param numerator
     */

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    /**
     * creates a string representation of the fraction
     * @return
     */
    public String show()
    {
        String frac = "";
        if (this.getDenominator() == 1)
        {
            if (this.getNumerator() != 1)
            {
                frac = Integer.toString(this.getNumerator());
            }
            else
            {
                frac = Integer.toString(1);
            }
        }
        else
        {
            frac = Integer.toString(this.getNumerator()) + "/" + Integer.toString(this.getDenominator());
        }
        return frac;
        }
}
