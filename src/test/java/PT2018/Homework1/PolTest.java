package PT2018.Homework1;

import  PT2018.Homework1.Model.*;


import org.junit.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import static junit.framework.Assert.assertEquals;
import org.junit.*;




public class PolTest {

    PolynomModel polynom1 = new PolynomModel("4x^3-7x^2-11x+5");
    PolynomModel polynom2 = new PolynomModel("4x+5");
    PolynomModel divpol = new PolynomModel("x^2-3x+1");
    PolynomModel addpol = new PolynomModel("4x^3-7x^2-7x+10");
    PolynomModel subpol = new PolynomModel("4x^3-7x^2-15x");
    PolynomModel mulpol = new PolynomModel("16x^4-8x^3-79x^2-35x+25");
    PolynomModel difpol = new PolynomModel("12x^2-14x-11");


    @Test
    public void checkDivison()
    {
        PolynomModel polynom4 = new PolynomModel(polynom1.divide(polynom2));
        assertEquals(polynom4.toString(),divpol.toString());
    }

    @Test
    public void checkAddition()
    {
        PolynomModel polynom4 = new PolynomModel(polynom1.add(polynom2));
        assertEquals(polynom4.toString(),addpol.toString());
    }

    @Test
    public void checkSubstraction()
    {
        PolynomModel polynom4 = new PolynomModel(polynom1.substract(polynom2));
        assertEquals(polynom4.toString(),subpol.toString());
    }

    @Test
    public void checkMultiplication()
    {
        PolynomModel polynom4 = new PolynomModel(polynom1.mul(polynom2));
        assertEquals(polynom4.toString(),mulpol.toString());
    }

    @Test
    public void checkDerivation()
    {
        polynom1.derivate();
        assertEquals(polynom1.toString(),difpol.toString());
    }






}
